import React from 'react'
import { DocsThemeConfig } from 'nextra-theme-docs'

const config: DocsThemeConfig = {
  logo: <span>numerous</span>,
  sidebar: {defaultMenuCollapseLevel: 1},
  docsRepositoryBase: 'https://gitlab.com/numerous/numerous-guides/-/blob/main/',
  footer: {
    text: '©️ Numerous',
  },
  useNextSeoProps() {
    return {
      titleTemplate: '%s – numerous docs'
    }
  },
}

export default config
