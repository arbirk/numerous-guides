# Welcome to the documentation repository for numerous guides

## How to contribute

### Edit on GitLab

You can edit directly on GitLab. Click on the **"Edit this page"** link on the right to go directly to that page in GitLab. On the edit button click the down arrow and **"Edit this file only".**

Edit the page and commit the change with a short commit message.

### Clone

You can clone the repository and push your changes directly to the main branch. This is not recommended for larger changes.

Be aware of potential merge conflicts if you push directly to the repo.

### Fork and Merge request

You can fork the repo and create merge requests from your own branches. This is recommended if you want to work locally or over several days.


Remember small commits are easy to make and don't cause merge conflicts.


## Local Development

Install node and pnpm

First, run `pnpm i` to install the dependencies.

Then, run `pnpm dev` to start the development server and visit localhost:3000.

## Nextra Docs Template 

The site is based on the [Nextra docs template](https://github.com/shuding/nextra-docs-template/).

It will be hosted at [docs.numerous.com](https://docs.numerous.com)

## Documentation
The documentation for the theme is available at [https://nextra.site/docs/docs-theme](https://nextra.site/docs/docs-theme).


